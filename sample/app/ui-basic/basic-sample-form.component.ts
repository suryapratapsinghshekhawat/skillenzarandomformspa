import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, FormArray } from "@angular/forms";
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {
    DynamicFormService,
    DynamicCheckboxModel,
    DynamicFormControlModel,
    DynamicFormArrayModel
} from "@ng-dynamic-forms/core";
import {
    DynamicCheckboxGroupModel,
    DynamicInputModel,
    DynamicRadioGroupModel,
    DynamicSelectModel,
    DynamicTextAreaModel,
    DynamicFormGroupModel
} from "@ng-dynamic-forms/core";
import { customValidator } from "../app.validators";
import { BASIC_SAMPLE_FORM_MODEL, BASIC_SAMPLE_FORM_ARRAY_MODEL } from "./basic-sample-form.model";
import { HttpClient } from '@angular/common/http';
@Component({
    moduleId: module.id,
    selector: "dynamic-basic-sample-form",
    templateUrl: "./basic-sample-form.component.html"
})
export class BasicSampleFormComponent implements OnInit {
    randomFormURL = "https://randomform.herokuapp.com/";
    formid: any;
    formname: any;
    formfields: any;
    formModel1: DynamicFormControlModel[];
    formModel2: DynamicFormControlModel[];

    formGroup1: FormGroup;
    formGroup2: FormGroup;

    checkboxControl: FormControl;
    checkboxModel: DynamicCheckboxModel;

    arrayControl: FormArray;
    arrayModel: DynamicFormArrayModel;

    constructor(private formService: DynamicFormService, private http: HttpClient) {

        //this.formModel1 = BASIC_SAMPLE_FORM_MODEL;
        //this.formModel2 = BASIC_SAMPLE_FORM_ARRAY_MODEL;

        this.formModel1 = this.formService.fromJSON(JSON.stringify(BASIC_SAMPLE_FORM_MODEL));
        this.formModel2 = this.formService.fromJSON(JSON.stringify(BASIC_SAMPLE_FORM_ARRAY_MODEL));

        this.formGroup1 = this.formService.createFormGroup(this.formModel1);
        this.formGroup2 = this.formService.createFormGroup(this.formModel2);
    }
    getRandomForm(): Observable<any> {
        return this.http.get(this.randomFormURL);
    }
    load() {
        this.getRandomForm().subscribe((data) => {
            console.log(data);
            let newfrm: any = [];
            data.data.form_fields.forEach(element => {
                console.log(element);

                switch (element.component) {
                    case "select":
                        let selectOptions: any[] = [];
                        element.options.forEach(opt => {
                            selectOptions.push({ value: opt, label: opt });
                        });
                        newfrm.push(
                            new DynamicSelectModel<string>({
                                id: "select" + selectOptions[0].value,
                                label: element.label,
                                options: selectOptions,
                                value: element.autoselect || selectOptions[0].value,
                                required: element.required,
                            })
                        );
                        break;
                    case "textarea":
                        newfrm.push(
                            new DynamicTextAreaModel({

                                id: element.label,
                                label: element.label,
                                rows: 5,
                                placeholder: element.description,
                                required: element.required,
                                validators: element.validation,
                                value: element.autofill,
                                disabled: element.editable

                            })
                        );
                        break;
                    case "checkbox":
                        let checkOptions: any[] = [];
                        element.options.forEach(opt => {
                            checkOptions.push(new DynamicCheckboxModel({
                                label: opt,
                                id: opt
                            }));
                        });
                        newfrm.push(
                            new DynamicFormArrayModel({

                                id: element.label,
                                label: element.label,
                                groupFactory: () => {
                                    return checkOptions;
                                }
                            })
                        );
                        break;
                    case "textinput":
                        newfrm.push(
                            new DynamicInputModel({

                                id: element.label,
                                label: element.label,
                                placeholder: element.description,
                                spellCheck: false,
                                required: element.required,
                                validators: element.validation,
                                value: element.autofill,
                                disabled: element.editable

                            })
                        );
                        break;
                    case "radio":

                        let selectRadioOptions: any[] = [];
                        element.options.forEach(opt => {
                            selectRadioOptions.push({ value: opt, label: opt });
                        });
                        newfrm.push(
                            new DynamicRadioGroupModel<string>({
                                id: element.label,
                                options: selectRadioOptions,
                                value: element.description,
                                disabled: element.editable,
                                required: element.required
                            })
                        );
                        break;


                    default:
                        break;
                }
            });
            console.log('adding to form model');

            this.formModel1 = this.formService.fromJSON(newfrm);
            this.formGroup1 = this.formService.createFormGroup(this.formModel1);
        });
    }
    ngOnInit() {

        // this.checkboxControl = this.formGroup1.controls["basicCheckbox"] as FormControl;
        // this.checkboxModel = this.formService.findById("basicCheckbox", this.formModel1) as DynamicCheckboxModel;

        // this.arrayControl = this.formGroup2.controls["basicFormArray"] as FormArray;
        // this.arrayModel = this.formService.findById("basicFormArray", this.formModel2) as DynamicFormArrayModel;
    }

    add() {
        this.formService.addFormArrayGroup(this.arrayControl, this.arrayModel);
    }

    remove(index: number) {
        this.formService.removeFormArrayGroup(index, this.arrayControl, this.arrayModel);
    }

    clear() {
        this.formService.clearFormArray(this.arrayControl, this.arrayModel);
    }

    onBlur($event) {
        console.log(`BLUR event on ${$event.model.id}: `, $event);
    }

    onChange($event) {
        console.log(`CHANGE event on ${$event.model.id}: `, $event);
    }

    onFocus($event) {
        console.log(`FOCUS event on ${$event.model.id}: `, $event);
    }
}